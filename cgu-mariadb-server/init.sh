#!/bin/bash
export PASSWORD=$PASSWORD
export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< "mariadb-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mariadb-server mysql-server/root_password_again password $PASSWORD" 

sudo apt-get update
sudo apt-get install -y mariadb-server mariadb-client
