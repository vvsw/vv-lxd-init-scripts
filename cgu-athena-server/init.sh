#!/bin/bash
ATHENA_SRC_PATH=/opt/athena/
mkdir -p $ATHENA_SRC_PATH

apt-get update
apt-get install git wget unzip gcc-5 g++-5 libmysqlclient-dev make
ln -s g++-5 /usr/bin/g++
ln -s gcc-5 /usr/bin/gcc
cd $ATHENA_SRC_PATH
wget {{cgu_athena_zip_url}} # https://gitlab.com/hoagieman/vv-eathena-mirror/-/archive/master/vv-eathena-mirror-master.zip
unzip vv-eathena-mirror-master.zip
cd vv-eathena-mirror-master
./configure --enable-64bit --with-MYSQL_LIBS=/usr/lib/x86_64-linux-gnu/libmysqlclient.so.20
make sql

